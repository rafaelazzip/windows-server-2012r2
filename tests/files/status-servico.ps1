#Esse script retorna o status de um determinado serviço
#Nesse script foi utilizado o serviço "Spooler"
$servico = Get-Service -Name "Spooler" -ComputerName "SRV01"
$servico.Status
if ($servico.Status -eq "Running") {
	echo "Parando o servico..."
	$servico.Stop()
	echo "Reiniciando o servico..."
	$servico.Start()
} else {
	echo "O servico nao estava executando!"
}
