Habilitar Ansible no Windows Server 2012 R2
=========

Esta pasta contém um exemplo de como habilitar o ansible no Windows-Server 2012 R2.


Pré-requesitos
------------

impacket/psexec.py
ansible>=2.7.2


powershell=>3.0


Habiliando o WINRM
----------------


Local:
Para copiar o arquivo .bat para dentro do host Windows e executá-lo, é necessário rodar o seguinte comando:

    psexec.py -c arquivo.bat dominio_ou_grupo/usuario_host:senha_host@"ip_ou_hostname"

      Exemplo: 
        > psexec.py -c script-winrm.bat WORKGROUP/USER:123456@"192.168.56.101"

    ou passar o mesmo comando na linha commando do playbook habilita_wirm.yml


Executando um playbook
---------------------

Após o passo anterior para executar algum playbook para testar se o WINRM foi habilitado corretamente:

  > ansible-playbook -i inventário playbook.yml

  ou se preferir:

  ansible all -i inventario -m win_ping


Observações
------------

- Para utilizar os playbooks localmente, utilizar o arquivo "inventory";

- Para utilizar os playbooks pelo Ansible AWX ou Ansible TOWER, utilizar o arquivo "/vars/vars_windwos.yml" 


- Esses playboks foram testados com as seguintes configurações/versões:

  - CentOS7
    - ansible 2.7.9
    - python 2.7.5
  - Windows Server 2012 R2pip install --upgrade setuptools
    - powershell 4.0


- Para instalar o impacket para utilizar o psexec siga os seguintes passos:

    > yum install python-pip
    > pip install --upgrade pip
    > pip install --upgrade setuptools
    > pip install impacket

Author Information
------------------

Rafael Azzi
rafaelazzipatricio@gmail.com
